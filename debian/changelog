libdatetime-format-http-perl (0.43-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.43.
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.7.0.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Nov 2024 01:57:43 +0100

libdatetime-format-http-perl (0.42-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 14 Oct 2022 12:42:01 +0100

libdatetime-format-http-perl (0.42-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 05 Jun 2022 17:31:36 +0100

libdatetime-format-http-perl (0.42-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 00:10:33 +0100

libdatetime-format-http-perl (0.42-2) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update Module::Build dependency.
  * Add debian/upstream/metadata
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Mon, 01 Jun 2015 18:25:38 +0200

libdatetime-format-http-perl (0.42-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * New upstream release.
  * Update years of packaging copyright.
  * Build-depend on Module::Build 0.38.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 May 2014 17:03:24 +0200

libdatetime-format-http-perl (0.40-1) unstable; urgency=low

  * New upstream release.
  * Use debhelper compat level 8.
  * debian/copyright: Refer to "Debian systems" instead of "Debian GNU/Linux
    systems"; refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Mon, 01 Nov 2010 11:53:51 +0100

libdatetime-format-http-perl (0.39-1) unstable; urgency=low

  * New upstream release.
  * Use Build.PL.
  * Use source format 3.0 (quilt).
  * Do no longer run maintainer tests. Remove build-dep on libtest-pod-perl,
    libtest-pod-coverage-perl.
  * Remove unused build-dep on libfile-find-rule-perl.
  * Bump Standards-Version to 3.9.0 (no changes).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 04 Jul 2010 00:55:51 +0900

libdatetime-format-http-perl (0.38-1) unstable; urgency=low

  [ Ryan Niebur ]
  * New upstream release
  * stop removing Makefile.PL, it's good now :)
  * enable all tests
  * Update ryan52's email address

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ gregor herrmann ]
  * debian/control: Changed: (build-)depend on perl instead of perl-
    modules.
  * Minimize debian/rules.
  * debian/control:
    - require debhelper 7.0.50 (override_* feature)
    - remove build dependency on Module::Build, there's a regular Makefile.PL
      now
    - remove versions from (build) dependencies on libdatetime-perl and
      libwww-perl
  * Update debian/copyright.
  * Set Standards-Version to 3.8.4 (no changes).

 -- Ernesto Hernández-Novich (USB) <emhn@usb.ve>  Mon, 01 Mar 2010 11:31:11 -0430

libdatetime-format-http-perl (0.37-1) unstable; urgency=low

  * Initial Release. (Closes: #519365)

 -- Ryan Niebur <ryanryan52@gmail.com>  Fri, 13 Mar 2009 00:23:03 -0700
